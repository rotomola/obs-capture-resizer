# OBS Capture Resizer
### Version 1.1 (September 30, 2021)

This is a Lua script for OBS Studio that provides a quick and smart way to resize capture sources automatically, and allows them to be saved as presets.

## Features

* Automatic resizing of multiple user-defined capture sources
* Sources can be scaled up to a user-defined maximum size
* Optional integer and user-defined scaling modes for preserving pixel fidelity
* Allows saving and loading of static resolution presets
* Optional animated effect
