obs = obslua

_anim_steps         = 6
_anim_decay         = 0.40
_anim_rebound_limit = 0.10

_min_width  = 16
_min_height = 16

g_anim_state    = "Stopped"
g_anim_cur_step = 0
g_target_w      = 0
g_target_h      = 0
g_outer_w       = 0
g_outer_h       = 0

g_resize_mode     = nil
g_output_width      = 0.0
g_output_height     = 0.0
g_output_max_width  = 0.0
g_output_max_height = 0.0
g_output_scale      = 0.0
g_integer_scaling   = true
g_animated          = false
g_auto_apply        = false

g_preset_description = ""
g_output_presets     = {}
g_current_preset     = ""

g_settings  = nil
g_props     = nil

function init()
end

function script_description()
	return "A convenient way to quickly resize the capture output source and save preset configurations."
end

function add_preset()
	if g_output_width == nil or g_output_height == nil then
		return
	end
	
	if g_preset_description == nil then
		g_preset_description = ""
	end
	
	local new_preset = { width = g_output_width, height = g_output_height, description = g_preset_description }
	local finished = false
	local insert_pos = 0
	for i,preset in pairs(g_output_presets) do
		insert_pos = insert_pos + 1
		if preset.width > g_output_width then
		elseif preset.width == g_output_width then
			if preset.height > g_output_height then
			elseif preset.height == g_output_height then
				--print("Updating preset description to: " .. g_preset_description)
				g_output_presets[i].description = g_preset_description
				finished = true
				break
			else
				table.insert(g_output_presets, i, new_preset)
				g_current_preset = tostring(insert_pos)
				finished = true
				break
			end
		else
			table.insert(g_output_presets, i, new_preset)
			g_current_preset = tostring(insert_pos)
			finished = true
			break
		end
	end
	if not finished then
		insert_pos = insert_pos + 1
		table.insert(g_output_presets, new_preset)
		g_current_preset = tostring(insert_pos)
	end
	
	obs.obs_data_set_string(g_settings, "output_preset", g_current_preset)
	save_presets_to_file()
	load_presets()
end

function load_presets()
	local presets_prop = obs.obs_properties_get(g_props, "output_preset")
	if presets_prop == nil then
		--print("load_presets: failed to retrieve presets property object")
		return
	end
	
	obs.obs_property_list_clear(presets_prop)
	
	for i,preset in pairs(g_output_presets) do
		local prop_text = preset.width .. "x" .. preset.height .. ": " .. preset.description
		local value_str = tostring(i)
		obs.obs_property_list_add_string(presets_prop, prop_text, value_str)
		--print("Adding preset to list: (" .. value_str .. ") " .. prop_text)
	end
end

function save_presets_to_file()
	local file, err = io.open(script_path() .. "/capture-resize-presets.txt", "w")
		if not file then
			--print("save_presets_to_file(): could not open file (" .. err .. ")")
			return nil, err
		end
		
	for i,preset in pairs(g_output_presets) do
		file:write(preset.width, ",", preset.height, ",", preset.description, "\n")
	end
	io.close(file)
end

function load_presets_from_file()
	local file, err = io.open(script_path() .. "/capture-resize-presets.txt", "r")
		if not file then
			--print("load_presets_from_file(): could not open file (" .. err .. ")")
			return nil, err
		end
	local contents = file:read("*a")
	io.close(file)
	
	g_output_presets = {}
	
	for line in io.lines(script_path() .. "/capture-resize-presets.txt") do
		local preset = {}
		local str_width, str_height, str_description = string.match(line, "([^,]+),([^,]+),(.*)")
		preset.width       = tonumber(str_width)
		preset.height      = tonumber(str_height)
		preset.description = tostring(str_description)
		if preset.width ~= nil and preset.height ~= nil then
			table.insert(g_output_presets, preset)
		end
	end
end

function apply_config()
	g_resize_mode        = obs.obs_data_get_string(g_settings, "resize_mode")
	g_output_max_width   = obs.obs_data_get_double(g_settings, "output_max_width")
	g_output_max_height  = obs.obs_data_get_double(g_settings, "output_max_height")
	g_output_scale       = obs.obs_data_get_double(g_settings, "output_scale")
	scene_name         = obs.obs_data_get_string(g_settings, "output_scene")
	g_integer_scaling    = obs.obs_data_get_bool(g_settings, "integer_scale")
	g_animated           = obs.obs_data_get_bool(g_settings, "animate")
	
	set_output_filter()
	
	if g_resize_mode == "manual" then
		set_output_size(g_output_width, g_output_height)
	end
end

function set_output_filter()
	local scene_source = obs.obs_get_source_by_name(scene_name)
	if scene_source == nil then
		--print("set_output_filter(): Failed to get scene source")
		return
	end
	
	local scene = obs.obs_scene_from_source(scene_source)
	
	local capture_sources_array = obs.obs_data_get_array(g_settings, "output_sources")
	local filter = tonumber(obs.obs_data_get_string(g_settings, "output_filters"))
	if filter == nil then
		obs.obs_source_release(scene_source)
		return
	end
	
	local array_count = obs.obs_data_array_count(capture_sources_array)
	for i = 1,array_count,1 do
		local data_item = obs.obs_data_array_item(capture_sources_array, i-1)
		if (data_item ~= nil) then
			local source_name = obs.obs_data_get_string(data_item, "value")
			local sceneItem = obs.obs_scene_find_source(scene, source_name)
			if sceneItem ~= nil then
				obs.obs_sceneitem_set_scale_filter(sceneItem, filter)
			end
			obs.obs_data_release(data_item)
		end
	end
	obs.obs_data_array_release(capture_sources_array)
	
	obs.obs_source_release(scene_source)
end

function set_output_size(width, height)
	if g_target_w == width and g_target_h == height then
		return
	end
	
	g_target_w = width
	g_target_h = height
	
	if g_animated then
		animate_start()
	else
		g_anim_cur_step = 0
		g_anim_state = "Stopped"
		update_output_size(width, height)
	end
end

function update_output_size(width, height)
	local scene_source = obs.obs_get_source_by_name(scene_name)
	if scene_source == nil then
		--print("update_output_size(): Failed to get scene source)
		return
	end
	
	local scene = obs.obs_scene_from_source(scene_source)
	local sources = get_output_sources()
	
	for i,source_name in pairs(sources) do
		local sceneItem = obs.obs_scene_find_source(scene, source_name)
		if sceneItem ~= nil then
			local bounds_size = obs.vec2()
			bounds_size.x = width
			bounds_size.y = height
			obs.obs_sceneitem_set_bounds(sceneItem, bounds_size)
		end
	end
	
	obs.obs_source_release(scene_source)
end

function get_output_sources()
	local sources = {}
	
	local capture_sources_array = obs.obs_data_get_array(g_settings, "output_sources")
	local array_count = obs.obs_data_array_count(capture_sources_array)
	for i = 1,array_count,1 do
		local data_item = obs.obs_data_array_item(capture_sources_array, i-1)
		if (data_item ~= nil) then
			local source_name = obs.obs_data_get_string(data_item, "value") -- "value" isn't documented for the items in editable_lists, but it works
			table.insert(sources, source_name)
			obs.obs_data_release(data_item)
		end
	end
	obs.obs_data_array_release(capture_sources_array)
	
	local unfiltered_sources_array = obs.obs_data_get_array(g_settings, "output_sources_unfiltered")
	array_count = obs.obs_data_array_count(unfiltered_sources_array)
	for i = 1,array_count,1 do
		local data_item = obs.obs_data_array_item(unfiltered_sources_array, i-1)
		if (data_item ~= nil) then
			local source_name = obs.obs_data_get_string(data_item, "value")
			table.insert(sources, source_name)
			obs.obs_data_release(data_item)
		end
	end
	obs.obs_data_array_release(unfiltered_sources_array)
	
	return sources
end

function animate_start()
	local scene_source = obs.obs_get_source_by_name(scene_name)
	if scene_source == nil then
		--print("animate_start(): Failed to get scene source")
		return
	end
	
	local scene = obs.obs_scene_from_source(scene_source)
	if scene == nil then
		--print("animate_start(): Failed to get scene")
		obs.obs_source_release(scene_source)
		return
	end
	
	local sources = get_output_sources()
	if table.getn(sources) <= 0 then
		obs.obs_source_release(scene_source)
		return
	end
	
	-- Get current position of the output sources
	local sceneItem = obs.obs_scene_find_source(scene, sources[1])
	if sceneItem == nil then
		--print("animate_start(): Failed to get scene item")
		obs.obs_source_release(scene_source)
		return
	end
	
	local bounds_size = obs.vec2()
	obs.obs_sceneitem_get_bounds(sceneItem, bounds_size)
	
	g_outer_w = bounds_size.x
	g_outer_h = bounds_size.y
	
	local x_diff = math.abs(g_target_w - g_outer_w)
	local y_diff = math.abs(g_target_h - g_outer_h)
	
	-- Set animation parameters
	if x_diff < 1 and y_diff < 1 then
		--print("animate_start(): Difference too small. Cancelling animation.")
		g_anim_state = "Stopped"
		update_output_size(g_target_w, g_target_h)
	else
		g_anim_state = "Approaching"
		g_anim_cur_step = 0
	end
	
	obs.obs_source_release(scene_source)
end

function animate_do()
	if g_anim_state ~= "Approaching" and g_anim_state ~= "Leaving" then
		return
	end
	
	if not g_animated and g_anim_state ~= "Stopped" then
		g_anim_state = "Stopped"
		g_anim_cur_step = 0
		update_output_size(g_target_w, g_target_h)
	end
	
	g_anim_cur_step = g_anim_cur_step + 1
	
	local x_diff = g_target_w - g_outer_w
	local y_diff = g_target_h - g_outer_h
	local new_w = g_target_w
	local new_h = g_target_h
	
	if g_anim_state == "Approaching" then
		local cur_pos = math.rad((g_anim_cur_step / _anim_steps) * 90)
		local delta = (-math.cos(cur_pos) + 1)
		
		new_w = g_outer_w + x_diff * delta
		new_h = g_outer_h + y_diff * delta
		
		if g_anim_cur_step == _anim_steps then
			-- Switch to "Leaving" and calculate
			g_anim_state = "Leaving"
			g_anim_cur_step = 0
			
			g_outer_w = g_target_w + x_diff * (1 - _anim_decay)
			g_outer_h = g_target_h + y_diff * (1 - _anim_decay)
			
			if x_diff > 0 then
				local limit_w = g_target_w * (1 + _anim_rebound_limit)
				if g_outer_w > limit_w then
					g_outer_w = limit_w
				end
			else
				local limit_w = g_target_w * (1 - _anim_rebound_limit)
				if g_outer_w < limit_w then
					g_outer_w = limit_w
				end
			end
			
			if y_diff > 0 then
				local limit_h = g_target_h * (1 + _anim_rebound_limit)
				if g_outer_h > limit_h then
					g_outer_h = limit_h
				end
			else
				local limit_h = g_target_h * (1 - _anim_rebound_limit)
				if g_outer_h < limit_h then
					g_outer_h = limit_h
				end
			end
		end
		
	elseif g_anim_state == "Leaving" then
		local cur_pos = math.rad(((_anim_steps - g_anim_cur_step) / _anim_steps) * 90)
		local delta = (-math.cos(cur_pos) + 1)
		
		new_w = g_outer_w + x_diff * delta
		new_h = g_outer_h + y_diff * delta
		
		if g_anim_cur_step == _anim_steps then
			animate_start()
			if g_anim_state == "Stopped" then
				return
			end
		end
	end
	
	update_output_size(new_w, new_h)
end

function script_tick(seconds)
	local scale_params = nil
	
	if g_resize_mode == "maximum" then
		scale_params = calculate_scale(nil, g_integer_scaling)
	elseif g_resize_mode == "scaled" then
		scale_params = calculate_scale(g_output_scale)
	end
	
	if scale_params ~= nil then
		if scale_params.width > 0 and scale_params.height > 0 then
			set_output_size(scale_params.width, scale_params.height)
		end
	end
	
	animate_do()
end

function calculate_scale(scale, integer_scale)
	local scale_params = {
		scale  = 0,
		width  = 0,
		height = 0
	}
	
	local  source_width = 0
	local source_height = 0
	local finished = false
	
	if scale ~= nil then
		scale_params.scale = scale
	end
		
	local scene_source = obs.obs_get_source_by_name(scene_name)
	if scene_source == nil then
		return scale_params
	end
	
	local scene = obs.obs_scene_from_source(scene_source)
		
	-- Get the list of capture sources
	local capture_sources_array = obs.obs_data_get_array(g_settings, "output_sources")
	local array_count = obs.obs_data_array_count(capture_sources_array)
	
	-- Look through each capture source and find a valid one to base our calculations off of
	for i = 1,array_count,1 do
		-- Pull the name of the source out of the list
		local data_item = obs.obs_data_array_item(capture_sources_array, i-1)
		if (data_item ~= nil) then
			-- Get the actual name out of the list and find the source
			local source_name = obs.obs_data_get_string(data_item, "value")
			local source = obs.obs_get_source_by_name(source_name)
			if source ~= nil then
				-- Find the scene item that uses this source
				scene_item = obs.obs_scene_find_source(scene, source_name)
				if scene_item ~= nil then
					-- If the scene item is not currently visible then this source is not valid for our calculations
					if obs.obs_sceneitem_visible(scene_item) then
						-- Get the raw size of the source
						source_width = obs.obs_source_get_width(source)
						source_height = obs.obs_source_get_height(source)
						if source_width >= _min_width and source_height >= _min_height then -- Caution: Capture cards tend to output sizes above zero, even if not active, hence the visibility check earlier
							-- If we've already decided on a scale, skip this section
							if scale == nil then
								-- Measure the difference between the source size and the maximum allowed dimensions
								scale_params.scale = math.min(g_output_max_width / source_width, g_output_max_height / source_height)
								-- If we're using integer scaling, we need to reduce the scale to an integer
								if integer_scale then
									scale_params.scale = math.floor(scale_params.scale)
								end
							end
							finished = true
						end
					end
				end
				obs.obs_source_release(source)
			end
			obs.obs_data_release(data_item)
		end
		
		-- Did we already find an appropriate source to measure from? If so, we don't need to check any others
		if finished then
			break
		end
	end
	obs.obs_data_array_release(capture_sources_array)
	obs.obs_source_release(scene_source)
	
	if scale_params.scale ~= nil then
		scale_params.width  =  source_width * scale_params.scale
		scale_params.height = source_height * scale_params.scale
	else
		scale_params.scale  = 0
		scale_params.width  = 0
		scale_params.height = 0
	end
	
	return scale_params
end

function script_properties()
	local props = obs.obs_properties_create()
	g_props = props

	--local p_output_sizes = obs.obs_properties_add_editable_list(props, "output_size_list", "Output Size", obs.OBS_EDITABLE_LIST_TYPE_STRINGS, nil, nil)
	local p_resize_mode = obs.obs_properties_add_list(props, "resize_mode", "Output Config", obs.OBS_COMBO_TYPE_LIST, obs.OBS_COMBO_FORMAT_STRING)
	obs.obs_property_list_add_string(p_resize_mode, "Auto-Maximum", "maximum")
	obs.obs_property_list_add_string(p_resize_mode, "Auto-Scaled", "scaled")
	obs.obs_property_list_add_string(p_resize_mode, "Manual", "manual")
	obs.obs_property_set_modified_callback(p_resize_mode, set_property_visibilities)
	
	obs.obs_properties_add_float(props, "output_max_width",  "Max Output Width",  0.0, 90000.0, 1.0)
	obs.obs_properties_add_float(props, "output_max_height", "Max Output Height", 0.0, 90000.0, 1.0)
	
	obs.obs_properties_add_bool(props, "integer_scale", "Use Integer Scaling")
	
	obs.obs_properties_add_float(props, "output_scale", "Scale", 0.0, 1000.0, 1.0)
	
	local p_output_preset = obs.obs_properties_add_list(props, "output_preset", "Preset", obs.OBS_COMBO_TYPE_LIST, obs.OBS_COMBO_FORMAT_STRING)
	load_presets()
	obs.obs_property_set_modified_callback(p_output_preset, set_preset_callback)
	
	obs.obs_properties_add_float(props, "output_width", "Width", 0.0, 90000.0, 1.0)
	obs.obs_properties_add_float(props, "output_height", "Height", 0.0, 90000.0, 1.0)
	obs.obs_properties_add_text(props, "output_preset_description", "Preset Description", obs.OBS_TEXT_DEFAULT)
	
	obs.obs_properties_add_button(props, "output_add_preset", "Update/Add Preset", add_preset_button_clicked)
	
	obs.obs_properties_add_button(props, "output_remove_preset", "Remove Preset", remove_preset_button_clicked)
	obs.obs_properties_add_button(props, "output_apply", "Apply", apply_button_clicked)
	obs.obs_properties_add_bool(props, "animate", "Animate")
	obs.obs_properties_add_bool(props, "auto_apply", "Auto-Apply")
	
	local p_output_filters = obs.obs_properties_add_list(props, "output_filters", "Output Filter", obs.OBS_COMBO_TYPE_LIST, obs.OBS_COMBO_FORMAT_STRING)
	obs.obs_property_list_add_string(p_output_filters, "Disable", tostring(obs.OBS_SCALE_DISABLE))
	obs.obs_property_list_add_string(p_output_filters, "Point", tostring(obs.OBS_SCALE_POINT))
	obs.obs_property_list_add_string(p_output_filters, "Bilinear", tostring(obs.OBS_SCALE_BILINEAR))
	obs.obs_property_list_add_string(p_output_filters, "Bicubic", tostring(obs.OBS_SCALE_BICUBIC))
	obs.obs_property_list_add_string(p_output_filters, "Lanczos", tostring(obs.OBS_SCALE_LANCZOS))
	obs.obs_property_list_add_string(p_output_filters, "Area", tostring(obs.OBS_SCALE_AREA))
	
	--obs.obs_properties_add_text(props, "output_scene", "Active Scene", obs.OBS_TEXT_DEFAULT)
	local p_scene = obs.obs_properties_add_list(props, "output_scene", "Active Scene", obs.OBS_COMBO_TYPE_EDITABLE, obs.OBS_COMBO_FORMAT_STRING)
	local scene_list = obs.obs_frontend_get_scene_names()
	for i,name in pairs(scene_list) do
		obs.obs_property_list_add_string(p_scene, name, name)
	end
	
	obs.obs_properties_add_editable_list(props, "output_sources", "Capture Sources", obs.OBS_EDITABLE_LIST_TYPE_STRINGS, nil, nil)
	obs.obs_properties_add_editable_list(props, "output_sources_unfiltered", "Additional Sources", obs.OBS_EDITABLE_LIST_TYPE_STRINGS, nil, nil)

	set_property_visibilities(props, nil, g_settings)
	set_preset_callback(props, nil, g_settings)
	
	return props
end

function script_update(settings)
	g_preset_description = obs.obs_data_get_string(settings, "output_preset_description")
	g_output_width       = obs.obs_data_get_double(settings, "output_width")
	g_output_height      = obs.obs_data_get_double(settings, "output_height")
	g_auto_apply         = obs.obs_data_get_bool(settings, "auto_apply")
	
	if g_auto_apply then
		apply_config()
	end
end

function set_property_visibilities(props, prop, settings)
	local g_resize_mode = obs.obs_data_get_string(settings, "resize_mode")
	
	local p_output_max_width = obs.obs_properties_get(props, "output_max_width")
	local p_output_max_height = obs.obs_properties_get(props, "output_max_height")
	local p_output_integer_scale = obs.obs_properties_get(props, "integer_scale")
	local p_output_scale = obs.obs_properties_get(props, "output_scale")
	local p_output_preset = obs.obs_properties_get(props, "output_preset")
	local p_output_preset_description = obs.obs_properties_get(props, "output_preset_description")
	local p_output_width = obs.obs_properties_get(props, "output_width")
	local p_g_output_height = obs.obs_properties_get(props, "output_height")
	local p_output_add_preset = obs.obs_properties_get(props, "output_add_preset")
	local p_output_remove_preset = obs.obs_properties_get(props, "output_remove_preset")
	
	obs.obs_property_set_visible(p_output_max_width, false)
	obs.obs_property_set_visible(p_output_max_height, false)
	obs.obs_property_set_visible(p_output_integer_scale, false)
	obs.obs_property_set_visible(p_output_scale, false)
	obs.obs_property_set_visible(p_output_preset, false)
	obs.obs_property_set_visible(p_output_preset_description, false)
	obs.obs_property_set_visible(p_output_width, false)
	obs.obs_property_set_visible(p_g_output_height, false)
	obs.obs_property_set_visible(p_output_add_preset, false)
	obs.obs_property_set_visible(p_output_remove_preset, false)
	
	if g_resize_mode == "maximum" then
		obs.obs_property_set_visible(p_output_max_width, true)
		obs.obs_property_set_visible(p_output_max_height, true)
		obs.obs_property_set_visible(p_output_integer_scale, true)
	elseif g_resize_mode == "scaled" then
		obs.obs_property_set_visible(p_output_scale, true)
	elseif g_resize_mode == "manual" then
		obs.obs_property_set_visible(p_output_preset, true)
		obs.obs_property_set_visible(p_output_preset_description, true)
		obs.obs_property_set_visible(p_output_width, true)
		obs.obs_property_set_visible(p_g_output_height, true)
		obs.obs_property_set_visible(p_output_add_preset, true)
		obs.obs_property_set_visible(p_output_remove_preset, true)
	end
	
	return true
end

function set_preset_callback(props, prop, settings)
	local preset_n = tonumber(obs.obs_data_get_string(settings, "output_preset"))
	if preset_n == nil then
		--print("set_preset_callback(): ID retrieved from dropdown is nil")
		return false
	end
	
	local preset = g_output_presets[preset_n]
	if preset == nil then
		--print("set_preset_callback(): Could not find preset: " .. tostring(preset_n))
		return false
	end
	
	obs.obs_data_set_string(settings, "output_preset_description", preset.description)
	obs.obs_data_set_double(settings, "output_width", preset.width)
	obs.obs_data_set_double(settings, "output_height", preset.height)
	g_current_preset = tostring(preset_n)
	
	script_update(settings)
	
	return true
end

function apply_button_clicked(props, p)
	apply_config()
	return false
end

function add_preset_button_clicked(props, p)
	add_preset()
	return true
end

function remove_preset_button_clicked(props, p)
	local preset_n = tonumber(g_current_preset)
	if preset_n == nil then
		return false
	end
	
	local count = 0
	for i,preset in pairs(g_output_presets) do
		count = count+1
	end
	table.remove(g_output_presets, preset_n)
	count = count - 1
	
	if (count == 0) then
		g_current_preset = ""
	elseif (count < preset_n) then
		g_current_preset = tostring(count)
	end
	
	obs.obs_data_set_string(g_settings, "output_preset", g_current_preset)
	save_presets_to_file()
	load_presets()
	set_preset_callback(props, nil, g_settings)
	
	return true
end

function script_defaults(settings)
	obs.obs_data_set_default_string(settings, "resize_mode", "manual")
	obs.obs_data_set_default_double(settings, "output_max_width", 1440.0)
	obs.obs_data_set_default_double(settings, "output_max_height", 1080.0)
	obs.obs_data_set_default_double(settings, "output_scale", 1.0)
	obs.obs_data_set_default_double(settings, "output_width", 1440.0)
	obs.obs_data_set_default_double(settings, "output_height", 1080.0)
	obs.obs_data_set_default_bool(settings,   "integer_scale", false)
	obs.obs_data_set_default_bool(settings,   "animate", false)
	obs.obs_data_set_default_bool(settings,   "auto-apply", false)
	obs.obs_data_set_default_string(settings, "output_filters", tostring(obs.OBS_SCALE_DISABLE))
end

function script_save(settings)
end

function script_load(settings)
	g_settings = settings
	g_current_preset = tonumber(obs.obs_data_get_string(settings, "output_preset"))
	
	load_presets_from_file()
	script_update(settings)
end
